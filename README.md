FBX Converter based on Adobe FBX SDK
http://www.autodesk.com/products/fbx/overview

Download, extract Python SDK.
python2 -m virtualenv venv_py2
cp lib/Python27_ucs4_x64/\* venv_py2/lib/python2.7/

Python3.5 versions seem to be missing Create() and other methods.

Python33
PYTHONPATH=lib/Python33_x64 python33
