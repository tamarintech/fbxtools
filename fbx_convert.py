#!/usr/bin/env python
import FbxCommon
import sys

def getavailableformats(ioregistry):
  formats = ioregistry.GetWriterFormatCount()
  for i in range(0, formats):
    print("{}: {}".format(i, ioregistry.GetWriterFormatDescription(i)))

  print("\n")

def print_nodes(node, indent):
    for space in range(indent):
        print(' ', end='')

    indent += 1

    nodetype = node.GetTypeName()

    print("{} ({}): {} children".format(node.GetName(),
                                   nodetype,
                                   node.GetChildCount()))

    for i in range(node.GetChildCount()):
        print_nodes(node.GetChild(i), indent)

manager = FbxCommon.FbxManager.Create()
ios = FbxCommon.FbxIOSettings.Create(manager, FbxCommon.IOSROOT)
manager.SetIOSettings(ios)
scene = FbxCommon.FbxScene.Create(manager, 'exportedScene')

getavailableformats(manager.GetIOPluginRegistry())

if len(sys.argv) < 4:
    print("{} <file> <mode> <outputfile>".format(sys.argv[0]))
    sys.exit(1)

modelfile = sys.argv[1]
savemode = sys.argv[2]
outputfile = sys.argv[3]

print("Loading {}".format(modelfile))

importer = FbxCommon.FbxImporter.Create(manager, '')
importer.Initialize(modelfile, -1, ios)
importer.Import(scene, False)
importer.Destroy()

print_nodes(scene.GetRootNode(), 0)

ios.SetBoolProp(FbxCommon.EXP_FBX_MATERIAL, True)
ios.SetBoolProp(FbxCommon.EXP_FBX_EMBEDDED, False)
exporter = FbxCommon.FbxExporter.Create(manager, '')

exporter.Initialize(outputfile, int(savemode), ios)
exporter.SetFileExportVersion(FbxCommon.FbxString('FBX201100'),
                              FbxCommon.FbxSceneRenamer.eNone)


result = exporter.Export(scene)
print("Export OK? {}".format(result))

exporter.Destroy()
