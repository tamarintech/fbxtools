#!/usr/bin/env python
import FbxCommon
import sys

def getavailableformats(ioregistry):
  formats = ioregistry.GetWriterFormatCount()
  for i in range(formats):
    print("{}: {}".format(i, ioregistry.GetWriterFormatDescription(i)))

def print_nodes(node, indent):
    for space in range(indent):
        print(' ', end='')

    indent += 1

    nodetype = node.GetTypeName()

    print("{} ({}): {} children".format(node.GetName(),
                                   nodetype,
                                   node.GetChildCount()))

    for i in range(node.GetChildCount()):
        print_nodes(node.GetChild(i), indent)

def rename_child(node, newname):
    node.SetName(node.GetName().replace('Bip001', newname))
    for i in range(node.GetChildCount()):
        rename_child(node.GetChild(i), newname)

if len(sys.argv) < 4:
    print("{} <model> <animation> <output>".format(sys.argv[0]))
    sys.exit(1)

modelfile = sys.argv[1]
animfile = sys.argv[2]
outputfile = sys.argv[3]

print("Merging {} and {}".format(modelfile, animfile))

manager = FbxCommon.FbxManager.Create()
ios = FbxCommon.FbxIOSettings.Create(manager, FbxCommon.IOSROOT)
manager.SetIOSettings(ios)
scene = FbxCommon.FbxScene.Create(manager, 'exportedScene')
scene1 = FbxCommon.FbxScene.Create(manager, 'importedScene1')
scene2 = FbxCommon.FbxScene.Create(manager, 'importedScene2')

### Load a single resource into the Scene
#result = FbxCommon.LoadScene(manager, scene, filepath)

#if not result:
#    print("Sorry, we were unable to load that file")
#    sys.exit(1)

importer = FbxCommon.FbxImporter.Create(manager, '')
importer.Initialize(modelfile, -1, ios)
importer.Import(scene1, False)
importer.Destroy()

importer = FbxCommon.FbxImporter.Create(manager, '')
importer.Initialize(animfile, -1, ios)
importer.Import(scene2, False)
importer.Destroy()

#scene1.RemoveNode(scene1.GetRootNode().FindChild('Bip001'))
#scene2.GetRootNode().FindChild('Bip001').SetName('Bip001Anim')

rename_child(scene2.GetRootNode().FindChild('Bip001'), 'Bip001Anim')

scene.AddRootMember(FbxCommon.FbxNode.Create(manager, 'Merged'))

for currscene in [scene1, scene2]:
    while currscene.GetRootNode().GetChildCount():
        scene.GetRootNode().AddChild(currscene.GetRootNode().GetChild(0))

    count = range(currscene.GetSrcObjectCount())
    for i in count:
        obj = currscene.GetSrcObject(i)

        if (obj == currscene.GetRootNode() or
           type(obj) == FbxCommon.FbxGlobalSettings or
           obj == currscene.GetGlobalSettings()):
            continue

        #print("Copying {}".format(obj.GetName()))
        obj.ConnectDstObject(scene)
    currscene.DisconnectAllSrcObject()
    currscene.GetRootNode().DisconnectAllSrcObject()

scene1.Destroy()
scene2.Destroy()

animstackcount = scene.GetSrcObjectCount(FbxCommon.FbxCriteria.ObjectType(FbxCommon.FbxAnimStack.ClassId))

for animi in range(animstackcount):
    animstack = scene.GetSrcObject(FbxCommon.FbxCriteria.ObjectType(FbxCommon.FbxAnimStack.ClassId), animi)
    layercount = animstack.GetSrcObjectCount(FbxCommon.FbxCriteria.ObjectType(FbxCommon.FbxAnimLayer.ClassId))

    for layeri in range(layercount):
        layer = animstack.GetSrcObject(FbxCommon.FbxCriteria.ObjectType(FbxCommon.FbxAnimLayer.ClassId), layeri)
        curvecount = layer.GetMemberCount()

        for curvei in range(curvecount):
            curve = layer.GetMember(curvei)

            propcount = curve.GetDstPropertyCount()

            tprops = []

            for propi in range(propcount):
                snode = curve.GetDstProperty(propi).GetFbxObject().GetName()
                tnode = scene.GetRootNode().FindChild(snode.replace('Anim', ''))

                if tnode is None:
                    continue

                tprops.append((curve.GetDstProperty(propi), tnode))

            for tprop in tprops:
                sprop, snode = tprop
                curve.ConnectDstProperty(FbxCommon.FbxProperty.CreateFrom(tnode, sprop))
                curve.DisconnectDstProperty(sprop)

scene.RemoveNode(scene.GetRootNode().FindChild('Bip001Anim'))

#print_nodes(scene.GetRootNode(), 0)

#ios.SetBoolProp(FbxCommon.EXP_FBX_MATERIAL, True)
#ios.SetBoolProp(FbxCommon.EXP_FBX_EMBEDDED, False)
exporter = FbxCommon.FbxExporter.Create(manager, '')

#getavailableformats(manager.GetIOPluginRegistry())

exporter.Initialize(outputfile, 0, ios)
exporter.SetFileExportVersion(FbxCommon.FbxString('FBX201100'),
                              FbxCommon.FbxSceneRenamer.eNone)

result = exporter.Export(scene)
print("Export OK? {}".format(result))

exporter.Destroy()
