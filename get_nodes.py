#!/usr/bin/env python
import FbxCommon
import sys

def print_nodes(node, indent):
    for space in range(indent):
        print(' ', end='')

    indent += 1

    nodetype = node.GetTypeName()

    print("{} ({}): {} children".format(node.GetName(),
                                   nodetype,
                                   node.GetChildCount()))

    for i in range(node.GetChildCount()):
        print_nodes(node.GetChild(i), indent)

if len(sys.argv) < 2:
    print('Path of FBX file required')
    sys.exit(1)

filepath = sys.argv[1]

print("Loading {}".format(filepath))

manager = FbxCommon.FbxManager.Create()
ios = FbxCommon.FbxIOSettings.Create(manager, FbxCommon.IOSROOT)
manager.SetIOSettings(ios)
scene = FbxCommon.FbxScene.Create(manager, 'importScene')

## Load a single resource into the Scene
result = FbxCommon.LoadScene(manager, scene, filepath)

if not result:
    print("Sorry, we were unable to load that file")
    sys.exit(1)

print_nodes(scene.GetRootNode(), 0)
