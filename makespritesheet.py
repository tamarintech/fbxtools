#!/usr/bin/env python

from PIL import Image
from sys import (argv, exit)
from math import ceil

if len(argv) < 3:
    print("usage: {} <output file> <input files>".format(argv[0]))
    exit(1)

output_file = argv[1]
input_files = argv[2:]

firstim = Image.open(argv[2])
size = firstim.size


print("Sprite size (from first image): {} x {} mode {}".format(size[0],
                                                               size[1],
                                                               firstim.mode))

columns = 8
rows = int(ceil(len(input_files)/columns))
newsheetsize = (firstim.size[0] * columns, firstim.size[1]*rows)

outputsheet = Image.new(firstim.mode, newsheetsize)

print("New sheet: {} x {} mode {}, {} frames".format(outputsheet.size[0],
                                                     outputsheet.size[1],
                                                     outputsheet.mode,
                                                     len(input_files)))

currcol = 0
currrow = 0

from collections import OrderedDict

jsonsheet = OrderedDict({ 'meta': {
    'app': 'Tamarin Sheetmaker',
    'image': output_file,
    'size': { 'w': outputsheet.size[0], 'h': outputsheet.size[1]},
    'scale': 1
    },
    'frames': OrderedDict({})
})

for i, infile in enumerate(input_files):
    im = Image.open(infile)
    cell = im.crop((0, 0, size[0], size[1]))
    outputsheet.paste(cell, (currcol*size[0], currrow*size[1]))
    im.close()

    frame = {"{}".format(output_file.replace('.png', str(i) + '.png')): {
        'frame': { 'x': currcol*size[0], 'y': currrow*size[1],
                   'w': size[0], 'h': size[1]},
        'rotated': False,
        'trimmed': True,
        'spriteSourceSize': { 'x': 0, 'y': 0,
                              'w': size[0], 'h': size[1]},
        'sourceSize': { 'w': size[0], 'h': size[1] }
    }}

    print("{}, {} complete".format(currrow, currcol))
    jsonsheet['frames'].update(frame)
    currcol += 1

    if currcol == columns:
        currrow += 1
        currcol = 0

outputsheet.save(argv[1])

import json

with open(output_file.replace('.png', '.json'), 'w') as f:
    json.dump(jsonsheet, f)
